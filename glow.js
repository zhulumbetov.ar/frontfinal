function createStars() {
    const numStars = 100; 
  
    const background = document.querySelector('.background');
    const width = background.offsetWidth;
    const height = background.offsetHeight;
  
    for (let i = 0; i < numStars; i++) {
      const star = document.createElement('div');
      star.className = 'star';
      star.style.left = Math.random() * width + 'px';
      star.style.top = Math.random() * height + 'px';
      background.appendChild(star);
    }
  }
  
  createStars();