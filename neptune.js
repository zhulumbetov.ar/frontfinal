document.addEventListener("DOMContentLoaded", function() {
    const rocket = document.getElementById("rocket");
    const obstacleContainer = document.getElementById("game-container");
    const timeSpan = document.getElementById("time");
  
    let gameInterval;
    let timeLeft = 20;
    let rocketPosition = 50;
    let obstaclePosition = window.innerWidth;
    let obstacleVerticalPositions = [];
  
    function startGame() {
      gameInterval = setInterval(updateGame, 10); // Increased speed by updating every 10 milliseconds
      updateTimer();
    }
  
    function updateGame() {
      obstaclePosition -= 6; // Increased speed by moving 10 pixels at a time
  
      if (obstaclePosition <= -50) {
        obstaclePosition = window.innerWidth;
        obstacleVerticalPositions.push(getRandomVerticalPosition());
        createObstacle();
      }
  
      rocket.style.top = rocketPosition + "%";
      const obstacles = document.querySelectorAll(".obstacle");
      obstacles.forEach(function(obstacle, index) {
        obstacle.style.top = obstacleVerticalPositions[index] + "%";
        obstacle.style.left = obstaclePosition + "px";
      });
  
      if (checkCollision()) {
        gameOver();
      }
    }
  
  
    function checkCollision() {
      const rocketRect = rocket.getBoundingClientRect();
      const obstacles = document.querySelectorAll(".obstacle");
      let collision = false;
  
      obstacles.forEach(function(obstacle) {
        const obstacleRect = obstacle.getBoundingClientRect();
  
        if (
          rocketRect.left < obstacleRect.right &&
          rocketRect.right > obstacleRect.left &&
          rocketRect.top < obstacleRect.bottom &&
          rocketRect.bottom > obstacleRect.top
        ) {
          collision = true;
        }
      });
  
      return collision;
    }
  
    function updateTimer() {
        timeSpan.textContent = timeLeft;
        timeLeft--;
      
        if (timeLeft < 0) {
          clearInterval(gameInterval);
          gameOver(true);
        } else {
          setTimeout(updateTimer, 1000);
        }
      }
      
      function gameOver(timerExpired) {
        clearInterval(gameInterval);
        rocket.style.backgroundColor = "#ff0000";
        const obstacles = document.querySelectorAll(".obstacle");
        obstacles.forEach(function(obstacle) {
          obstacle.style.backgroundColor = "#ff0000";
        });
      
        if (timerExpired) {
          // Display "Good job! You have escaped." message and redirect after 3 seconds
          const message = document.createElement("div");
          message.classList.add("message");
          message.textContent = "Good job! You have escaped.";
      
          setTimeout(function() {
            document.body.appendChild(message);
            setTimeout(function() {
              window.location.href = "index.html";
            }, 3000);
          }, 1000);
        } else {
          // Display "You are dead." message
          const message = document.createElement("div");
          message.classList.add("message");
          message.textContent = "You are dead.";
          document.body.appendChild(message);
        }
      }
      
      
  
    function getRandomVerticalPosition() {
      const maxPosition = 80; // Adjust this value to change the range of vertical positions
      return Math.floor(Math.random() * maxPosition) + 10; // Starting from 10% to avoid very low positions
    }
  
    function createObstacle() {
      const obstacle = document.createElement("div");
      obstacle.classList.add("obstacle");
      obstacleContainer.appendChild(obstacle);
    }
  
    document.addEventListener("keydown", function(event) {
      if (event.key === "w" || event.key === "W") {
        rocketPosition -= 1;
  
        if (rocketPosition < 0) {
          rocketPosition = 0;
        }
      } else if (event.key === "s" || event.key === "S") {
        rocketPosition += 1;
  
        if (rocketPosition > 100) {
          rocketPosition = 100;
        }
      }
    });
  
    createObstacle();
    startGame();
  });
  