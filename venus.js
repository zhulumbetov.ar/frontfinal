document.addEventListener("DOMContentLoaded", function() {
  var numAliens = 10;

  for (var i = 0; i < numAliens; i++) {
    createAlien();
  }

  function createAlien() {
    var alien = document.createElement("div");
    alien.classList.add("alien");
    alien.style.top = getRandomPosition() + "px";
    alien.style.left = getRandomPosition() + "px";
    alien.style.transform = "scale(" + getRandomScale() + ")";

    document.getElementById("planet").appendChild(alien);
  }

  function getRandomPosition() {
    var planetHeight = document.getElementById("planet").offsetHeight;
    var minPosition = planetHeight * 0.5;
    var maxPosition = planetHeight * 0.9;
    return Math.random() * (maxPosition - minPosition) + minPosition;
  }

  function getRandomScale() {
    var minScale = 1.1;
    var maxScale = 1.5;
    return Math.random() * (maxScale - minScale) + minScale;
  }
});

function goBack() {
  window.location.href = "index.html";
}