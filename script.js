// Позиция и ротация
const rocket = document.getElementById("rocket");

let rocketX = 0;
let rocketY = 0;

function moveRocket(x, y) {
  rocketX += x;
  rocketY += y;

  rocket.style.transform = `translate(${rocketX}px, ${rocketY}px)`;

  if (x > 0 && y === 0) {
    rocket.style.transform += " rotate(45deg)";
  } else if (x < 0 && y === 0) {
    rocket.style.transform += " rotate(225deg)";
  } else if (x === 0 && y > 0) {
    rocket.style.transform += " rotate(135deg)";
  } else if (x === 0 && y < 0) {
    rocket.style.transform += " rotate(315deg)";
  }

  // ховер
  const planets = document.getElementsByClassName("planet");
  for (let i = 0; i < planets.length; i++) {
    const planet = planets[i];
    const planetPosition = planet.getBoundingClientRect();
    const rocketPosition = rocket.getBoundingClientRect();

    if (
      rocketPosition.left >= planetPosition.left &&
      rocketPosition.right <= planetPosition.right &&
      rocketPosition.top >= planetPosition.top &&
      rocketPosition.bottom <= planetPosition.bottom
    ) {
      planet.style.transform = "scale(1.2)";
      planet.classList.add("hovered"); 
      const info = planet.querySelector(".info");
      info.style.display = "block";
      info.innerHTML = "Do you want to enter this planet? Press Enter.";
    } else {
      planet.style.transform = "scale(1)";
      planet.classList.remove("hovered"); 
      const info = planet.querySelector(".info");
      info.style.display = "none";
    }
  }
}

// WASD
document.addEventListener("keydown", function (event) {
  const key = event.key.toLowerCase();

  switch (key) {
    case "w":
      moveRocket(0, -10); 
      break;
    case "a":
      moveRocket(-10, 0); 
      break;
    case "s":
      moveRocket(0, 10); 
      break;
    case "d":
      moveRocket(10, 0); 
      break;
    case "enter":
        const hoveredPlanet = document.querySelector(".planet.hovered");
        if (hoveredPlanet) {
          const planetId = hoveredPlanet.id;
          if (planetId === "sun") {
            document.body.style.backgroundColor = "red"; 
            document.body.innerHTML = "<h1 style='margin-top: 250px;'>YOU ARE BURNT ALIVE</h1>"; 
          } else if (planetId === "jupiter" || planetId === "saturn") {
            document.body.style.backgroundColor = "green"; 
            document.body.innerHTML = "<h1 style='margin-top: 250px;'>ITS A GASEOUS PLANET</h1>"; 
          } else {
            window.location.href = `${planetId}.html`; 
          }
        }
  break;
      
  
  }
});

///////
// не работает
function getQueryParameter(parameterName) {
  const queryString = window.location.search;
  const urlParams = new URLSearchParams(queryString);
  return urlParams.get(parameterName);
}
//не работает без локалсторейджа и вебпака, я старался
function goHome() {
  const planetParam = getQueryParameter("planet");
  if (planetParam) {
    const rocket = document.getElementById("rocket");
    const planet = document.getElementById(planetParam);

    if (planet) {
      if (planetParam === "earth") {
        rocket.style.top = "30%";
        rocket.style.left = "20%";
        rocket.style.marginTop = "-20px";
        rocket.style.marginLeft = "-20px";
      } else {
        const planetPosition = planet.getBoundingClientRect();
        rocket.style.transform = `translate(${planetPosition.left}px, ${planetPosition.top}px)`;
        rocket.style.zIndex = "1"; 
      }
    }
  }

  localStorage.setItem("currentPlanetId", "earth");

  window.location.href = "index.html";
}

//////

// Hostage speech animation
const speechElement = document.getElementById("speech");
const speechText = "You are a hostage. Answer three questions and we will free you.";
let speechIndex = 0;

function animateSpeech() {
  if (speechIndex < speechText.length) {
    speechElement.textContent += speechText.charAt(speechIndex);
    speechIndex++;
    setTimeout(animateSpeech, 50);
  } else {
    const proceedButton = document.createElement("button");
    proceedButton.textContent = "Proceed to Test";
    proceedButton.style.backgroundColor = "#32a852";
    proceedButton.style.color = "white";
    proceedButton.style.fontSize = "20px";
    proceedButton.style.padding = "10px 20px";
    proceedButton.style.border = "none";
    proceedButton.style.borderRadius = "5px";
    proceedButton.style.cursor = "pointer";
    proceedButton.style.position = "relative";
    proceedButton.style.animation = "alienate 2s infinite";
    proceedButton.addEventListener("click", () => {
      window.location.href = "quiz.html";
    });
    document.body.appendChild(proceedButton);
  }
}


animateSpeech();
////////




