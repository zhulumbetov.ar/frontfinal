const quizQuestions = [
    {
      question: "2 + 2 = ?",
      options: ["4", "3", "5", "2"],
      answer: "4"
    },
    {
      question: "Capital of Pakistan?",
      options: ["Karachi", "Lahore", "Islamabad", "Peshawar"],
      answer: "Islamabad"
    },
    {
      question: "What is benzene?",
      options: ["Element", "Metal", "Compound", "Gas"],
      answer: "Compound"
    }
  ];
  
  const questionContainer = document.getElementById("question-container");
  const optionsContainer = document.getElementById("options-container");
  const submitButton = document.getElementById("submit-btn");
  const message = document.getElementById("message");
  const body = document.body;
  
  let currentQuestion = 0;
  let score = 0;
  let mistakes = 0;
  
  function displayQuestion() {
    const question = quizQuestions[currentQuestion];
    questionContainer.textContent = question.question;
  
    optionsContainer.innerHTML = "";
    for (let i = 0; i < question.options.length; i++) {
      const option = document.createElement("div");
      option.classList.add("option");
      option.textContent = question.options[i];
      option.addEventListener("click", selectOption);
      optionsContainer.appendChild(option);
    }
  }
  
  function selectOption(event) {
    const selectedOption = event.target;
    const question = quizQuestions[currentQuestion];
  
    if (selectedOption.textContent === question.answer) {
      selectedOption.style.backgroundColor = "#00cc00";
      score++;
    } else {
      selectedOption.style.backgroundColor = "#ff0000";
      mistakes++;
    }
  
    currentQuestion++;
  
    if (currentQuestion < quizQuestions.length) {
      setTimeout(displayQuestion, 1000);
    } else {
      setTimeout(() => {
        if (mistakes >= 1) {
          body.style.backgroundColor = "#ff0000";
          body.innerHTML = "<h1 style='margin-top: 250px;'>YOU ARE DEAD</h1>";
        } else if (score === quizQuestions.length) {
          displayCongratulatoryMessage();
        } else {
          message.textContent = "You scored " + score + " out of " + quizQuestions.length;
        }
      }, 1000);
    }
  }
  
  function displayCongratulatoryMessage() {
    const congratsMessage = "Congratulations! Correct! You are free genious!";
    const congratsContainer = document.createElement("div");
    congratsContainer.classList.add("congrats-container");
    const congratsText = document.createElement("h1");
    congratsText.classList.add("congrats-text");
    congratsContainer.appendChild(congratsText);
    document.body.appendChild(congratsContainer);
  
    let charIndex = 0;
    const typingSpeed = 100; 
  
    function typeCongratsMessage() {
      if (charIndex < congratsMessage.length) {
        congratsText.textContent += congratsMessage.charAt(charIndex);
        charIndex++;
        setTimeout(typeCongratsMessage, typingSpeed);
      } else {
        congratsContainer.appendChild(aliensContainer);
      }
    }
  
    typeCongratsMessage();
    setTimeout(() => {
        window.location.href = "index.html";
      }, 10000);
  }
  
  submitButton.addEventListener("click", displayQuestion);
  
  displayQuestion();
  