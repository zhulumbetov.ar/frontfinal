document.addEventListener('DOMContentLoaded', function() {
    var daimonDialogueElement = document.getElementById('daimon-dialogue');
    var youDialogueElement = document.getElementById('you-dialogue');
    var userInput = document.getElementById('user-input');
    var submitBtn = document.getElementById('submit-btn');

    var currentQuestion = 0;
    var questions = [
        {
            question: "How are you?",
            validResponses: ["good", "bad"],
            response: ["Math Daimon: I'm feeling ", " too!"]
        },
        {
            question: "What are you doing?",
            validResponses: [],
            response: ["Math Daimon: I'm doing nothing, just enjoying the cosmic wonders!"]
        },
        {
            question: "Do you want to explore with me?",
            validResponses: ["yes", "no"],
            responseYes: "Math Daimon: Absolutely! Let's embark on an exciting journey together.",
            responseNo: "Math Daimon: That's alright. If you change your mind, I'll be here."
        }
    ];

    function displayQuestion(questionIndex) {
        var currentQuestionObj = questions[questionIndex];
        daimonDialogueElement.innerHTML = "Math Daimon: " + currentQuestionObj.question;
        userInput.disabled = false;
        submitBtn.disabled = false;
        userInput.focus();
    }

    displayQuestion(currentQuestion);

    function processUserResponse(userResponse) {
        var currentQuestionObj = questions[currentQuestion];
        if (currentQuestionObj.validResponses.length > 0) {
            if (!currentQuestionObj.validResponses.includes(userResponse.toLowerCase())) {
                daimonDialogueElement.innerHTML = "Math Daimon: I'm sorry, I didn't understand your response.";
                return;
            }
        }
        youDialogueElement.innerHTML += "<p>You: " + userResponse + "</p>";

        if (currentQuestionObj.question === "How are you?") {
            daimonDialogueElement.innerHTML = "Math Daimon: " + currentQuestionObj.response[0] + userResponse + currentQuestionObj.response[1];
        } else if (currentQuestionObj.question === "What are you doing?") {
            daimonDialogueElement.innerHTML = currentQuestionObj.response[0];
        } else if (currentQuestionObj.question === "Do you want to explore with me?") {
            if (userResponse.toLowerCase() === "yes") {
                daimonDialogueElement.innerHTML = currentQuestionObj.responseYes;
                userInput.disabled = true;
                submitBtn.disabled = true;
                setTimeout(function() {
                    window.location.href = "index.html";
                }, 3000);
                return;
            } else if (userResponse.toLowerCase() === "no") {
                daimonDialogueElement.innerHTML = currentQuestionObj.responseNo;
                setTimeout(function() {
                    window.location.href = "index.html";
                }, 3000);
            }
        }

        currentQuestion++;
        if (currentQuestion < questions.length) {
            setTimeout(function() {
                displayQuestion(currentQuestion);
            }, 2000);
        } else {
            userInput.disabled = true;
            submitBtn.disabled = true;
        }
    }

    submitBtn.addEventListener('click', function() {
        var userResponse = userInput.value.trim();
        if (userResponse !== '') {
            processUserResponse(userResponse);
            userInput.value = '';
        }
    });
});
